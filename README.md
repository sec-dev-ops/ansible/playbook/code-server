**Install requirements**

ansible-galaxy install -r requirements.yaml

**Run playbook**

ansible-playbook -i \<INVENTORY\> code-server.pbk.yaml